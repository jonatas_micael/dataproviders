package generator.cpfGenerator;

import java.util.Random;

public class CpfGenerator {

	public static String getCPF() {

		Random random = new Random();
		long valor = random.nextLong();
		
		String strCPF = (String.valueOf((valor < 0 ? valor * -1 : valor))).substring(0, 9);
		int Soma = 0;
		int Resto = 0;
		Soma = 0;	
		for (int i = 1; i <= 9; i++) {
			Soma = Soma + Integer.parseInt(strCPF.substring(i - 1, i)) * (11 - i);
		}

		Resto = (Soma * 10) % 11;
		
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;		

		strCPF += Resto;
		for (int i = 1; i <= 9; i++) {
			Soma += Integer.parseInt(strCPF.substring(i - 1, i));		
		}
		Soma += Integer.parseInt(strCPF.substring(9, 10)) * 2;
		
		Resto = (Soma * 10) % 11;
		
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;
		
		strCPF += Resto;
		
		return strCPF;
	}
}
