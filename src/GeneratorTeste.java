import generator.cpfGenerator.CpfGenerator;
import generators.peopleGenerator.Person;
import generators.peopleGenerator.PeopleGenerator;

public class GeneratorTeste {

	public static void main(String[] args) {
//		System.out.println(CpfGenerator.getCPF());
		Person people = PeopleGenerator.getNewPerson();
		System.out.println("Sex: " + people.getSex().getSexType());
		System.out.println("FirstName: " + people.getName().getFirstName());
		System.out.println("LastName: " + people.getName().getLastName());
		System.out.println("Email: " + people.getEmail().getFullEmail());
		System.out.println("Password: " + people.getEmail().getPassword());
		System.out.println("DayOfMonth: " + people.getDateOfBirth().getDayOfMonth());
		System.out.println("Month: " + people.getDateOfBirth().getMonth());
		System.out.println("Year: " + people.getDateOfBirth().getYear());
		System.out.println("AddressName: " + people.getAddress().getAddressName());
		System.out.println("City: " + people.getAddress().getCity());
		System.out.println("State: " + people.getAddress().getState());
		System.out.println("ZipCode: " + people.getAddress().getZipCode());
		System.out.println("Country: " + people.getAddress().getCountry());
		System.out.println("Number: " + people.getAddress().getNumber());
		System.out.println("NumberPhone: " + people.getNumberPhone());		
	}
}
