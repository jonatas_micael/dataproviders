package generators.nameGenerator;

public class NameFormats {

	public static String FullName = "{f} {f} {l} {l}";
	public static String SimpleName = "{f} {l}";
	public static String FistName = "{f}";
	public static String LastName = "{l}";
	public static String NickName = "{n}";

}
