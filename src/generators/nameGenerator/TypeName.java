package generators.nameGenerator;

public enum TypeName {	
	FirstName("{f}"), 
	LastName("{l}");

	private String format;
	
	private TypeName( String format ) {
		this.format = format;
	}

	public String getFormat() {		
		return format;
	}
}
