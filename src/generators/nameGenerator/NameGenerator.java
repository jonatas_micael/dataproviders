package generators.nameGenerator;

import java.util.Random;

import generators.nameGenerator.Name.Builder;

public class NameGenerator {

	private static Random random = new Random();
		
//	public static String newName(String format) {
//		 while(format.contains(TypeName.FirstName.getFormat()))
//				format.replace(TypeName.FirstName.getFormat(), NameGenerator.newFirstName());
//		 
//		 while(format.contains(TypeName.LastName.getFormat()))
//				format.replace(TypeName.LastName.getFormat(), NameGenerator.newFirstName());
//		 
//		 return format;
//	}
	
	public static Name newFullName(String format) {
		String name;
		Builder builder = new Name.Builder(); 
		while(format.contains(TypeName.FirstName.getFormat())) {
			name = NameGenerator.newFirstName();
			format = replaceFirst(format, TypeName.FirstName.getFormat(), name);
			builder.firstName(name);
		}
		
		while(format.contains(TypeName.LastName.getFormat())) {
			name = NameGenerator.newFirstName();
			format = replaceFirst(format, TypeName.LastName.getFormat(), name);
			builder.lastName(name);
		}
		
		return builder.build();
	}
	
	private static String replaceFirst(String format, String delimiter, String change) {
		String newString = "";
		if (format.contains(delimiter)) {
			int index = format.indexOf(delimiter);
			newString += format.substring(0, index);
			newString += change;
			newString += format.substring(index + delimiter.length());		
		}
		return newString;
	}

	private static String newFirstName() {	
		int totalLength = random.nextInt(14) + 1;
		String name = String.valueOf((char)(random.nextInt(25) + 65));		
		while ( totalLength > name.length() ) 
			name += String.valueOf((char)(random.nextInt(25) + 97));
		
		return name;
	}

//	NotImplemented
//	public static String newSimpleName() {
//		while(format.contains(TypeName.FirstName.getFormat()))
//			format.replace(TypeName.FirstName.getFormat(), NameGenerator.newFirstName());
//		
//		while(format.contains(TypeName.LastName.getFormat()))
//			format.replace(TypeName.LastName.getFormat(), NameGenerator.newSimpleName());		
//	}

}
