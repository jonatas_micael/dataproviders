package generators.nameGenerator;

import java.util.ArrayList;
import java.util.List;

public class Name {
	
	private List<String> firstName = new ArrayList<String>();
	private List<String> lastName = new ArrayList<String>();
	private List<String> nickname = new ArrayList<String>();
	private String names;
	
	public static class Builder {
		private List<String> firstName = new ArrayList<String>();
		private List<String> lastName = new ArrayList<String>();
		private List<String> nickname = new ArrayList<String>();
		
		public Builder firstName(String firstName) {
			this.firstName.add(firstName);
			return this;
		}
		
		public Builder lastName(String lastName) {
			this.lastName.add(lastName);
			return this;
		}
		
		public Builder nickname(String nickname) {
			this.nickname.add(nickname);
			return this;
		}
		
		public Builder() {
			
		}
		
		public Name build() {
			return new Name(this);
		}
	}
	
	private Name( Builder builder ) {
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
		this.nickname =	builder.nickname;
	}
	
	public String getFirstName() {
		names = "";
		firstName.forEach(name -> names += name + " ");
		return names.trim();
	}
	
	public String getLastName() {
		names = "";
		lastName.forEach(name -> names += name + " ");
		return names.trim();
	}
	
	public String getNickname() {
		names = "";
		nickname.forEach(name -> names += name);
		return names;
	}
	
	public String getFullName() {
		names = "";
		firstName.forEach(name -> names += name + " ");
		lastName.forEach(name -> names += name + " ");		
		return names.trim();
	}
}
