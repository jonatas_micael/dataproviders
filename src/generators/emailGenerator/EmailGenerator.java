package generators.emailGenerator;

import java.util.Random;

import generators.emailGenerator.Email;
import generators.emailGenerator.EmailGenerator;
import generators.emailGenerator.Email.Builder;
import generators.nameGenerator.NameGenerator;
import generators.nameGenerator.TypeName;
import generators.emailGenerator.TypeEmail;

public class EmailGenerator {
	private static Random random = new Random();
	
//	public static String newEmail(String format) {
//		 while(format.contains(TypeEmail.FirstEmail.getFormat()))
//				format.replace(TypeEmail.FirstEmail.getFormat(), EmailGenerator.newFirstEmail());
//		 
//		 while(format.contains(TypeEmail.LastEmail.getFormat()))
//				format.replace(TypeEmail.LastEmail.getFormat(), EmailGenerator.newFirstEmail());
//		 
//		 return format;
//	}
	
	public static Email newEmail(String format) {
		String email;
		Builder builder = new Email.Builder(); 
		
		while(format.contains(TypeEmail.User.getFormat())) {
			email = EmailGenerator.newUserEmail();
			format = replaceFirst(format, TypeEmail.User.getFormat(), email);
			builder.user(email);
		}
			
		email = EmailGenerator.newLastDomainEmail();
		format = replaceLast(format, TypeEmail.Domain.getFormat(), email);
		builder.domain(email);
		
		while(format.contains(TypeEmail.Domain.getFormat())) {	
			email = EmailGenerator.newDomainEmail();
			format = replaceLast(format, TypeEmail.Domain.getFormat(), email);
			builder.domain(email);		
		}
		
		builder.password(newPassword(TypePassword.AllCharacters));
		
		return builder.build();
	}
	
	private static String replaceFirst(String format, String delimiter, String change) {
		String newString = "";
		if (format.contains(delimiter)) {
			int index = format.indexOf(delimiter);
			newString += format.substring(0, format.indexOf(delimiter));
			newString += change;
			newString += format.substring(index + delimiter.length());		
		}
		return newString;
	}
	
	private static String replaceLast(String format, String delimiter, String change) {
		String newString = "";
		if (format.contains(delimiter)) {
			int index = format.lastIndexOf(delimiter);
			newString += format.substring(0, index);
			newString += change;
			newString += format.substring(index + delimiter.length());		
		}
		return newString;
	}
	
	private static String newUserEmail() {	
		int totalLength = random.nextInt(14) + 1;
		String email = "";	
		while ( totalLength > email.length() ) 
			email += String.valueOf((char)(random.nextInt(25) + 97));
		
		return email;
	}
	
	private static String newDomainEmail() {	
		int totalLength = random.nextInt(24) + 2;
		String email = "";	
		while ( totalLength > email.length() ) 
			email += String.valueOf((char)(random.nextInt(25) + 97));
		
		return email;
	}
	
	private static String newLastDomainEmail() {	
		int totalLength = random.nextInt(1) + 2;
		String email = "";	
		while ( totalLength > email.length() ) 
			email += String.valueOf((char)(random.nextInt(25) + 97));
		
		return email;
	}
	
	private static String newPassword(TypePassword typePassword) {	
		int totalLength = random.nextInt(16) + 8;
		String password = "";	
		while ( totalLength > password.length() ) 
			password += String.valueOf((char)(random.nextInt(93) + 33));
		
		return password;
	}
}
