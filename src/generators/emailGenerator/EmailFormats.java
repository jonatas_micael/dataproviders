package generators.emailGenerator;

public class EmailFormats {
	
	public static String UDD = "{u}@{d}.{d}";
	public static String UDDD = "{u}@{d}.{d}.{d}";
	public static String UUDD = "{u}.{u}@{d}.{d}";
	public static String UUDDD = "{u}.{u}@{d}.{d}.{d}";	
	public static String UUUDDD = "{u}.{u}@{d}.{d}.{d}";
	public static String UUUDDDD = "{u}.{u}@{d}.{d}.{d}";
}
