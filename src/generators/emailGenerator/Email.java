package generators.emailGenerator;

import java.util.ArrayList;
import java.util.List;

public class Email {

	private List<String> user = new ArrayList<String>();
	private List<String> domain = new ArrayList<String>();
	private String password;
	private String allUser = "";
	private String allDomain = "";
	
	public Email(Builder builder) {
		this.user = builder.user;
		this.domain = builder.domain;
		this.password = builder.password;
	}

	public static class Builder {
		
		private List<String> user = new ArrayList<String>();
		private List<String> domain = new ArrayList<String>();
		private String password;
		
		public Builder user(String user) {
			this.user.add(user);
			return this;
		}

		public Builder domain(String domain) {
			this.domain.add(domain);
			return this;
		}
		
		public Builder password(String password) {
			this.password = password;
			return this;
		}

		public Email build() {			
			return new Email(this);
		}
	}
	
	public String getDomain() {
		allDomain = "";
		domain.forEach(domain -> allDomain = "." + domain + allDomain);
		return allDomain.substring(1);
	}
	
	public String getUser() {
		allUser = "";
		user.forEach(user -> allUser += user + ".");
		return allUser.substring(0, allUser.length() - 1);
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getFullEmail() {
		return getUser() + "@" + getDomain();
	}
}
