package generators.emailGenerator;

public enum TypePassword {
	LowerCaseLetters("{l}"),
	UpperCaseLetters("{u}"),
	AllLetters("{al}"), 
	AllLettersAndNumbers("{an}"),
	NumbersAndSymbols("{ln}"),
	AllLettersAndSymbols("{as}"),
	AllCharacters("{c}");

	private String format;
	
	private TypePassword( String format ) {
		this.format = format;
	}

	public String getFormat() {		
		return format;
	}
}
