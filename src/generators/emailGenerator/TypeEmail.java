package generators.emailGenerator;

public enum TypeEmail {	
	User("{u}"), 
	Domain("{d}");

	private String format;
	
	private TypeEmail( String format ) {
		this.format = format;
	}

	public String getFormat() {		
		return format;
	}
}
