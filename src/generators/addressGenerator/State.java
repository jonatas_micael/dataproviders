package generators.addressGenerator;

public class State {

	private String stateName;
	
	public State(String state) {
		stateName = state;
	}
	
	public String getStateName() {
		return stateName;
	}
}
