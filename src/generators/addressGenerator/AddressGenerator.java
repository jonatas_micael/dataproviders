package generators.addressGenerator;

import java.util.Random;

public class AddressGenerator {

	private static Random random = new Random();
	
	public static Address newAddress(Country country) {
		return new Address.Builder(country)
			.city(newCityGenerator())
			.state(newStateGenerator(country))
			.number(newNumberGenerator())
			.zipCode(newZipCodeGenerator())
			.address(newAddressGenerator())
			.build();		
	}
	
	private static String newAddressGenerator() {		
		String name = "";
		int totalLength = 0;
		int words = random.nextInt(3) + 2;
		
		for (int i = 0; i < words; i++) {
			
			totalLength = random.nextInt(15) + 5;
			
			name += String.valueOf((char)(random.nextInt(25) + 65));		
		
			while ( totalLength > name.length() ) 
				name += String.valueOf((char)(random.nextInt(25) + 97));			
			
			name += " ";
		}
		return name.trim();		
	}
	
	private static String newCityGenerator() {		
		String name = "";
		int totalLength = 0;
		int words = random.nextInt(3) + 1;
		
		for (int i = 0; i < words; i++) {
			
			totalLength = random.nextInt(15) + 5;
			
			name = String.valueOf((char)(random.nextInt(25) + 65));		
		
			while ( totalLength > name.length() ) 
				name += String.valueOf((char)(random.nextInt(25) + 97));			
			
			name += " ";
		}
		return name.trim();		
	}
	
	private static State newStateGenerator(Country country) {		
		return country.getStates().get(random.nextInt(country.getStates().size()));
	}
	
	private static String newNumberGenerator() {		
		return String.valueOf(random.nextInt(3000));		
	}
	
	private static String newZipCodeGenerator() {		
		int totalLength = 7;		
		String name = "";				
		while ( totalLength > name.length() ) 
			name += String.valueOf(random.nextInt(9));
		
		return name;		
	}
}
