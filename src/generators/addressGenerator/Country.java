package generators.addressGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import generators.addressGenerator.State;

public enum Country {
	
	UnitedStates("Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "Washington, D.C.", "West Virginia", "Wisconsin", "Wyoming"),
	Brazil("");
	
	private List<State> states = new ArrayList<State>();
	
	private Country( String... states ) {
		Arrays.asList(states).forEach( state -> this.states.add(new State(state)));
	}
	
	public List<State> getStates() {
		return states;
	}

}
