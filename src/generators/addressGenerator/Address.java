package generators.addressGenerator;

public class Address {
	
//	private City city;
	private State state;
//	private ZipCode zipCode;
	private Country country;	
	private String city;	
	private String zipCode;
	private String number;
	private String address;
	
	public static class Builder {
		private Country country;	
		private String city;
		private State state;
		private String zipCode;
		private String number;
		private String address;
		
		public Builder city(String city) {
			this.city = city;
			return this;
		}
		
		public Builder state(State state) {
			this.state = state;
			return this;
		}
		
		public Builder zipCode(String zipCode) {
			this.zipCode = zipCode;
			return this;
		}
		
		public Builder number(String number) {
			this.number = number;
			return this;
		}
		
		public Builder address(String address) {
			this.address = address;
			return this;
		}
		
		public Builder(Country country) {
			this.country = country;
		}
		
		public Address build() {
			return new Address(this);
		}
	}
	
	private Address( Builder builder ) {
		this.country = builder.country; 
		this.city = builder.city;
		this.state = builder.state;
		this.zipCode = builder.zipCode;
		this.number = builder.number;
		this.address = builder.address;
	}
	
	public String getCity() {
		return city;
	}
	 
	public Country getCountry() {
		return country;
	}
	
	public String getState() {
		return state.getStateName();
	}
	
	public String getZipCode() {
		return zipCode;
	}
	
	public String getNumber() {
		return number;
	}
	
	public String getAddressName() {
		return address;
	}
}
