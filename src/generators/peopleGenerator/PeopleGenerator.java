package generators.peopleGenerator;

import java.time.LocalDate;
import java.util.Random;

import generators.addressGenerator.AddressGenerator;
import generators.addressGenerator.Country;
import generators.emailGenerator.EmailFormats;
import generators.emailGenerator.EmailGenerator;
import generators.nameGenerator.NameFormats;
import generators.nameGenerator.NameGenerator;

public class PeopleGenerator {

	public static Person getNewPerson() {

		Random random = new Random();
		
		return new Person.Builder(NameGenerator.newFullName(NameFormats.FullName))
			.address(AddressGenerator.newAddress(Country.UnitedStates))
			.email(EmailGenerator.newEmail(EmailFormats.UUDD))
			.sex(Sex.values()[random.nextInt(1)])
			.numberPhone(String.valueOf(Math.abs(random.nextLong())).substring(0, 11))
			.dateOfBirth(LocalDate.now().minusYears(random.nextInt(81) + 18))
			.build();
	}

}
