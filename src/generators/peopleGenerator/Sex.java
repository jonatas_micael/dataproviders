package generators.peopleGenerator;

public enum Sex {
	
	Male("Male"),
	Female("Female");
	
	private String sexType;
	
	private Sex(String sexType) {
		this.sexType = sexType;
	}
	
	public String getSexType() {
		return sexType;
	}

}
