package generators.peopleGenerator;

import java.time.LocalDate;

import generators.addressGenerator.Address;
import generators.emailGenerator.Email;
import generators.nameGenerator.Name;

public class Person {
	
//	private Parents parents;
//	private String cpf;
	private Name name; 
	private Sex sex;
	private Email email;
	private String numberPhone;
	private Address address;
	private LocalDate dateOfBirth;
	
	public static class Builder {
		
		//Required 
		private Name name; 
		
		//Optional
//		private Parents parents;
//		private NumberPhone numberPhone;
//		private String cpf;		
		private String numberPhone;
		private Sex sex;
		private Email email;
		private Address address;
		private LocalDate dateOfBirth;			
		
		public Builder sex(Sex sex) {
			this.sex = sex;
			return this;
		}
		public Builder address(Address address) {
			this.address = address;
			return this;
		}
		
		public Builder dateOfBirth(LocalDate dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
			return this;
		}
		
		public Builder email(Email email) {
			this.email = email;
			return this;
		}
		
		public Builder numberPhone(String numberPhone) {
			this.numberPhone = numberPhone;
			return this;
		}
		
		public Builder(Name name) {
			this.name = name;
		}
		
		public Person build() {			
			return new Person(this);
		}
	}
	
	private Person(Builder builder) {
//		Parents parents;
//		this.cpf = builder.cpf;
		this.name = builder.name; 
		this.sex = builder.sex;
		this.email = builder.email;
		this.numberPhone = builder.numberPhone;
		this.address = builder.address;
		this.dateOfBirth = builder.dateOfBirth;	
	}	
	
	public Address getAddress() {
		return address;
	}
	
	public Email getEmail() {
		return email;
	}
	
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	
	public String getNumberPhone() {
		return numberPhone;
	}
	
	public Name getName() {
		return name;
	}
	
	public Sex getSex() {
		return sex;
	}	
}
